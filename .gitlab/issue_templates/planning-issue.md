<!-- 
Title should be:

Monitor ##.# Planning issue
-->

### Planning Boards

[Monitor Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1131777?label_name[]=group%3A%3Amonitor)

### Goals for the milestone:

<!-- Replace these with the high-level goals for the milestone -->
*  Goal 1...
*  Goal 2...

### Scope of Work for Engineering

<!--
List of issues in priority order go in this table
Add t-shirt size for Frontend and/or Backend if there is work for that part of the team

Row format: 
|1|[]()| | | | | | |
-->

| Priority | Issue | Notes | Assigned | In Dev | Merged | Frontend | Backend |
|----------|-------|----------|----|----|----|---------|---------|
| |[]()| | | | | | |
| |[]()| | | | | | |
| |[]()| | | | | | |
| |[]()| | | | | | |
| |[]()| | | | | | |

### Scope of Work for UX

<!-- What work will UX be focused on? -->

| Issue | When it should be ready |
|-------|-------------------------|
|       |                         |

### Scope of Work for Testing

<!-- What work will Testing be focused on? -->

| Issue | Investigates/Tests | Due on |
|-------|--------------------|--------|
|       |                    |        |

/label ~"group::monitor" ~"devops::monitor" ~"section::ops" ~"Planning Issue" 
/assign @crystalpoole @kbychu
